- 👋 Hi, I’m @shubmehetre
- 👀 I’m interested in Computer, networking, security, scripting, also Football, computer games, Reading :)
- 🌱 I’m currently learning Ethical Hacking
- 💞️ I’m looking to collaborate on Open source projects, CTFs
- 📫 How to reach me @github: github.com/shubmehetre @mail: shub.mehetre@gmail.com

<!---
shubmehetre/shubmehetre is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
